import { SETTINGS_MODES } from '@/data/SettingsModes'
import SettingsStore from '@/store/SettingsStore'
import { useSnapshot } from 'valtio'

export default function SettingsModePicker() {
  const { mode } = useSnapshot(SettingsStore.state)

  function onSelect(value: string) {
    SettingsStore.setMode(value)
  }

  return (
    <select
      value={mode}
      onChange={e => onSelect(e.currentTarget.value)}
      aria-label="SettingsModes"
    >
      {SETTINGS_MODES.map((endpoint, index) => {
        return (
          <option key={index} value={endpoint.value}>
            {endpoint.label}
          </option>
        )
      })}
    </select>
  )
}