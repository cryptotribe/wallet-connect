import { proxy } from 'valtio'

/**
 * Types
 */
interface State {
  account: number
  mode: string
  eip155Address: string
  cosmosAddress: string
  solanaAddress: string
  polkadotAddress: string
  nearAddress: string
  elrondAddress: string
  relayerRegionURL: string
}

/**
 * State
 */
const state = proxy<State>({
  account: 0,
  mode: '',
  eip155Address: '',
  cosmosAddress: '',
  solanaAddress: '',
  polkadotAddress: '',
  nearAddress: '',
  elrondAddress: '',
  relayerRegionURL: ''
})

/**
 * Store / Actions
 */
const SettingsStore = {
  state,

  setAccount(value: number) {
    state.account = value
  },

  setMode(value: string) {
    state.mode = value
  },

  setEIP155Address(eip155Address: string) {
    state.eip155Address = eip155Address
  },

  setCosmosAddress(cosmosAddresses: string) {
    state.cosmosAddress = cosmosAddresses
  },

  setSolanaAddress(solanaAddress: string) {
    state.solanaAddress = solanaAddress
  },

  setPolkadotAddress(polkadotAddress: string) {
    state.polkadotAddress = polkadotAddress
  },

  setNearAddress(nearAddress: string) {
    state.nearAddress = nearAddress
  },
  
  setRelayerRegionURL(relayerRegionURL: string) {
    state.relayerRegionURL = relayerRegionURL
  },

  setElrondAddress(elrondAddress: string) {
    state.elrondAddress = elrondAddress
  },

}

export default SettingsStore
