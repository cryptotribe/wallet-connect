/**
 * Types
 */

type ModeType = {
  value: string
  label: string
}

// Settings Mode
export const SETTINGS_MODES: ModeType[] = [
  {
    value: 'Create',
    label: 'Create new account'
  },
  {
    value: 'Export',
    label: 'Export account'
  },
  {
    value: 'Import',
    label: 'Import account'
  }
]