import EIP155Lib from '@/lib/EIP155Lib'

export let wallet: EIP155Lib
export let eip155Wallets: Record<string, EIP155Lib>
export let eip155Addresses: string[]


/**
 * Create new EIP155 wallet
 */
export function createEIP155Wallet() {
  // Init new wallet
  wallet = EIP155Lib.init({})
  // Don't store mnemonic in local storage in a production project!
  localStorage.setItem('EIP155_MNEMONIC', wallet.getMnemonic())

  let address = wallet.getAddress()

  eip155Wallets = {
    [address]: wallet
  }
  eip155Addresses = Object.keys(eip155Wallets)

  return {
    eip155Wallets,
    eip155Addresses
  }
}

/**
 * Restore EIP155 wallet from mnemonic
 */
export function restoreEIP155Wallet(mnemonic: string) {

  wallet = EIP155Lib.init({ mnemonic: mnemonic })

  // Don't store mnemonic in local storage in a production project!
  localStorage.setItem('EIP155_MNEMONIC', wallet.getMnemonic())

  let address = wallet.getAddress()

  eip155Wallets = {
    [address]: wallet
  }
  eip155Addresses = Object.keys(eip155Wallets)

  return {
    eip155Wallets,
    eip155Addresses
  }
}

/**
 * Load existing wallets from localStorage
 */
export function loadEIP155Wallet() {

  const mnemonic = localStorage.getItem('EIP155_MNEMONIC')
  console.log('EIP155_MNEMONIC', mnemonic)

  wallet = EIP155Lib.init({ mnemonic: mnemonic })

  // Don't store mnemonic in local storage in a production project!
  localStorage.setItem('EIP155_MNEMONIC', wallet.getMnemonic())

  let address = wallet.getAddress()

  eip155Wallets = {
    [address]: wallet
  }
  eip155Addresses = Object.keys(eip155Wallets)

  return {
    eip155Wallets,
    eip155Addresses
  }
}
