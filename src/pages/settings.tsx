import PageHeader from '@/components/PageHeader'
import RelayRegionPicker from '@/components/RelayRegionPicker'
import SettingsModePicker from '@/components/SettingsModePicker'
import SettingsStore from '@/store/SettingsStore'
import { cosmosWallets } from '@/utils/CosmosWalletUtil'
import { eip155Wallets, createEIP155Wallet, restoreEIP155Wallet } from '@/utils/EIP155WalletUtil'
import { solanaWallets } from '@/utils/SolanaWalletUtil'
import { elrondWallets } from '@/utils/ElrondWalletUtil'
import { Card, Divider, Row, Switch, Text, Textarea, Button } from '@nextui-org/react'
import { Fragment, useState } from 'react'
import { useSnapshot } from 'valtio'
import packageJSON from '../../package.json'

export default function SettingsPage() {
  const { testNets, mode, eip155Address, cosmosAddress, solanaAddress, elrondAddress } = useSnapshot(
    SettingsStore.state
  )
  const [mnemonic, setMnemonic] = useState('')

  function onAccountCreate() {
    createEIP155Wallet()
    window.location.reload()
  }

  function onAccountImport() {
    if(mnemonic) {
      restoreEIP155Wallet(mnemonic)
      window.location.reload()
    }
  }

  return (
    <Fragment>
      <PageHeader title="Settings" />

      <Row justify="space-between" align="center">
        <Text h4 css={{ marginBottom: '$5' }}>
          Relayer Region
        </Text>
        <RelayRegionPicker />
      </Row>

      <Divider y={2} />

      <Row justify="space-between" align="center">
        <Text h4 css={{ marginBottom: '$5' }}>
          Pick Action
        </Text>
        <SettingsModePicker />
      </Row>

      <Divider y={2} />

      <Text h4 css={{ marginTop: '$5', marginBottom: '$5' }}>
        EIP155 Mnemonic ({mode})
      </Text>

      {(!mode || mode=='Create') ? (
        <Fragment>
          <Button
            color="gradient"
            css={{ marginTop: '$10', width: '100%' }}
            onClick={onAccountCreate}>
            Create new account
          </Button>
        </Fragment>
      ) : null}
    
      {(mode=='Import') ? ( 
       <Fragment> 
        <Card bordered borderWeight="light" css={{ minHeight: '100px' }}>
          <Textarea 
            onChange={(e) => setMnemonic(e.target.value)}
            label="Import by mnemonic"
            placeholder="Enter 12 word mnemonic"/>
        </Card>
        <Button
            color="gradient"
            css={{ marginTop: '$10', width: '100%' }}
            onClick={onAccountImport}>
            Import account
        </Button>
       </Fragment>
      ) : null}

      {(mode=='Export') ? ( 
        <Fragment>
          <Text h4 css={{ marginTop: '$5', marginBottom: '$5' }}>
            Export and backup mnemonic for account {eip155Address}
          </Text>
          <Card bordered borderWeight="light" css={{ minHeight: '100px' }}>
            <Text css={{ fontFamily: '$mono' }}>{eip155Wallets[eip155Address].getMnemonic()}</Text>
          </Card>
        </Fragment>
      ) : null}
      

    </Fragment>
  )
}
