FROM node:18.13.0
COPY . /usr/app/
WORKDIR /usr/app
RUN yarn install
EXPOSE 3001
ENTRYPOINT ["npm", "run","dev"]
