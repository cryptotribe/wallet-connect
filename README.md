# Wallet Connect - Web Wallet (React, Typescript, Ethers, NextJS, Cosmos)

**The wallet implements Wallet Connect v1 and v2 for several chains**
Project forked and modified based of original sample wallet project:
https://github.com/WalletConnect/web-examples/blob/main/wallets/react-wallet-v2 


## Build and run project

1. Install dependencies `yarn install` or `npm install`

2. Run `yarn dev` or `npm run dev` to start wallet

